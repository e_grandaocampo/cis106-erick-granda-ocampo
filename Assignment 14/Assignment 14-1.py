#My program takes a reads a file and finds the min,max and average of a list of scores.

import os
import sys


def read_file(filename):
    try: 
        file = open(filename, "r")
        next(file)
        grades = []
        for line in file:
            line = line.strip()
            if not line.find(",") == 0:
                grades.append(int(line[line.find(",")+1:]))
        file.close()
        return grades
    except:
        print("Error reading", filename)
        print(sys.exc_info()[1])


def get_max(grades):
    maximum = max(grades)
    return maximum


def get_min(grades):
    minimum = min(grades)
    return minimum


def get_average(grades):
    average = sum(grades) / len(grades)
    return average


def display_all(grades, maximum, minimum, average):
    print(grades)
    print("The maximum grade is " + str(maximum))
    print("The minimum grade is " + str(minimum))
    print("The average grade is " + str(average))


def main():
    filename = "scores.txt"
    grades = read_file(filename)
    maximum = get_max(grades)
    minimum = get_min(grades)
    average = get_average(grades)
    display_all(grades, maximum, minimum, average)


main()



#References
http://deeplearning.lipingyang.org/2017/01/23/read-file-from-line-2-or-skip-header-row-in-python/
#https://stackoverflow.com/questions/4796764/read-file-from-line-2-or-skip-header-row