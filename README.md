Erick Granda-Ocampo

# Session 1

I chose to use python, since it was recommended as a great beginners coding language. I am in this class to hopefully learn a lot of new ways to use a computer. Also, I am going for the two-year degree for electronics. I intend to use python in my future endeavor, and also as a starting base for my knowledge of coding. 

# Session 2

Using flowgorithm has become a lot easier with the practice the hands-on activities requires. Adding comments took a second to figure out and seeing how they appear on each of the different language programs was enticing. Using cloud9 was confusing at first, but with other students help I was able to edit my comments and then save them back to my Bitbucket. I have seen that coding requires a lot of time and patience to figure each new activity. After time using Cloud9, it did not seem as intimidating as when I first saw it. 

# Session 3 

I learned how to create a real program using flowgorithm. It was challenging at first, but with patience I slowly began to understand what each command did and the process for each variable. It was a lot of fun when the program began to work. I plan to use this to further my career as a future electrical engineer. I may not know exactly how learning coding will directly correlate with my future in electrical engineering. I am sure that learning how to code will make me more valuable in the workplace. Also, as an electrical engineering I will be creating new ideas and understanding coding will aid me in that process.

# Session 4

During this session I began to understand coding with python a lot more than I did during the previous session. I looked up what an electrical engineer does, and I could be doing a lot of different things. However, working with robots are one area of expertise and that was very interesting. What gave me some trouble is figuring out some of the commands to push my work from cloud 9 back to bitbucket. I use git push in cloud 9, but it does not appear in my bitbucket. 

# Session 5

How to use functions was the focus of this lesson. I got a good grasp of what function is and how to use them. However, I was not sure how many I was supposed to use and if there was a rule to follow of how many were needed, or when was needed instead of a direct line of code. Hopefully to hear feedback answering these questions. My classmate also had a similar question. This second time around I understood the material a lot better, and hopefully did all the functions correctly. 

# Session 6

In session 6 I learned how to use functions on python. Starting a new program always seems like the hardest part of the process. I also learned some new coding language, it was not very hard to get the structure of the program with the aid of the flowgorithm example. While I am still not one hundred percent sure that the order of coding is correct. I did use what I felt to be the best flow to the program. 

# Session 7

In this session we added onto what we learned in the last few sessions. We learned how to use the �If� command as part of our code. This seemed complicated to me at first, but the book had good examples to follow. Using functions really helped keep everything organized and easier to follow, and allowed any errors to be fixed easily. 

# Session 8

While an unfortunate week has prevented me from posting regularly. I have a chance to learn what and how to use the While function on flowgorithm. It allows to do several functions repeatedly without having several lines of code. Instead an accurate expression will be able to continually do the math accurately. While I do not know a lot about robots just yet, I do know learning this before starting the higher-level engineering courses will be very beneficial. 

# Session 9

This week I learned about the second type of while source code. While I did learn a lot from the first while program. This one had its own problems. I learned that this do while code processes all information at the end of the process. I will catch up with everything that I am behind on this week so I will not fall behind again. Learning how to use while loops will help automate future program what will need several inputs without creating long lines of code. An example could be using a budgeting calculator and other simple mathematic programs. 

# Session 10

This session I learned how to use nested loops and for loops in our programing languages. An approach I had to learn the for loop first was like the other loops we did previously. However, the hardest part was to figure out how to use include another loop into our program. Learning this will be a great help moving forward in my education since nested looping will allow more than one process to happen in a single program opening the doors for more complicated programs. 

# Session 11

In this session we began to learn about arrays and how to use them to make a list of numbers. While also compiling them into a certain equation that we need them to go through. One way this could be related to the real world is that teachers use a certain program similar to the one I created to get data back on a test and on students did. Without having to do all the number crunching each time certain information is needed. 

# Session 12

I began to write code a lot faster than I was writing even in the previous session. Writing the arrays format was the trickiest part of this program. However, I began to plan out the bulk of the program in my head and then I would focus on the array after everything above it makes sense. 

# Session 13

In this session we began using more function that change strings. In my program I used the split, replace functions so far. I know I will need to use more for the other programs. Those two were simple enough to use and did not cause too much trouble. I see these functions being very useful when processing information and replacing raw data with more information added and being more useful overall. 

# Session 14

When I first began this session is seemed really confusing to me at first. The more I read into the chapter the more sense is made I also saw how the last chapter tied directly into this next assignment.  I see how this tie into real life because every time a program is ran, the file is having to be read then displayed. All the programs that are made to do the math for my electronic courses should be a file containing the formulas. 

# Session 16

For the final project everything came everything we learned in this semester came together and each part of the previous lessons was needed to complete this project. I fine tuned my understand on how to use f statement and the find and strip methods. I know this class will help in my future plc courses since we will be running software with the code, we will be writing for it. Handling errors was also a pretty tough spot for me since making a slight mistake could prevent the whole programming from running. So taking it slow was crucial to ensure everything would still run with the error handling code integrated into the program. 

