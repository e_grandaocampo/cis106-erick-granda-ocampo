#My program spilts a set of scores into separate lines


def get_numbers():
    print("Please enter your values/words with commas separating them.")
    numbers = str(input())

    return numbers
    
def get_strip(numbers):
    strip = numbers.strip()
    
    return strip
    
def get_final(strip):
    final = strip.replace(',', '\n')
    
    return final


def display_all(numbers, strip, final):
    print(numbers)
    print(strip) 
    print(final)
    

def main():
    numbers = get_numbers()
    strip = get_strip(numbers)
    final = get_final(strip)
    display_all(numbers, strip, final)
    
    
main()  



#references 
#https://www.programiz.com/python-programming/string