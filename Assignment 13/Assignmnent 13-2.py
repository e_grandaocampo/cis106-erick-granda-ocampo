#My program put a line of text backwards

def get_expression():
    print("please enter an expression")
    expression = str(input())
    
    return expression
    
    
def get_strip(expression):
    strip = expression.strip()
    
    return strip
    
def get_final(strip):
    final = strip[::-1]
    
    return final
    
def display_all(expression, strip, final):
    print(expression)
    print(strip)
    print(final)
    
    
def main():
    expression = get_expression()
    strip = get_strip(expression)
    final = get_final(strip)
    display_all(expression, strip, final)
    
    
main()