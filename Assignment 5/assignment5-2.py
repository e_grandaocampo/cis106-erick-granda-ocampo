def display(hours, payRate, weeklyPay, monthlyPay, annualPay):
    print("Your weekly gross income is $" + str(weeklyPay))
    print("Your monthly gross pay is $" + str(monthlyPay))
    print("Your annual gross pay is $" + str(annualPay))

def getAnnualPay(hours, payRate):
    annualPay = hours * payRate * 52
    
    return annualPay

def getHours():
    print("How many hours did you work this week?")
    hours = int(input())
    
    return hours

def getMonthlyPay(hours, payRate):
    monthlyPay = hours * payRate * 4
    
    return monthlyPay

def getPayRate():
    print("How much do you make an hour?")
    payRate = int(input())
    
    return payRate

def getWeeklyPay(hours, payRate):
    weeklyPay = hours * payRate
    
    return weeklyPay

# Main

# My program calculates you gross weekly income, gross monthly income and gross annual income.
payRate = getPayRate()
hours = getHours()
weeklyPay = getWeeklyPay(hours, payRate)
monthlyPay = getMonthlyPay(hours, payRate)
annualPay = getAnnualPay(hours, payRate)
display (hours, payRate, weeklyPay, monthlyPay, annualPay)
