def calAgeSec(ageYears):
    secAge = ageYears * 31536000
    
    return secAge

def calDaysAge(ageYears):
    ageDays = ageYears * 365
    
    return ageDays

def calHourAge(ageYears):
    hourAge = ageYears * 8760
    
    return hourAge

def calMonths(ageYears):
    ageMonths = ageYears * 12
    
    return ageMonths

def display(ageYears, ageDays, ageMonths, secAge, hourAge):
    print("Your age in days is " + str(ageDays))
    print("Your age in months is " + str(ageMonths))
    print("Your age in hours is " + str(hourAge))
    print("Your age in seconds is " + str(secAge))

def getAge():
    print("Please Enter your age")
    ageYears = int(input())
    
    return ageYears

# Main

# My program calculate your age in months, day, hours and minutes.
ageYears = getAge()
ageDays = calDaysAge(ageYears)
ageMonths = calMonths(ageYears)
secAge = calAgeSec(ageYears)
hourAge = calHourAge(ageYears)
display (ageYears, ageDays, ageMonths, secAge, hourAge)
