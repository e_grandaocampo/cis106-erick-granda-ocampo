def calculate_overtime(hours, payrate):
    overtimepay = payrate * 40 + payrate * (hours - 40) * 1.5
    
    
    return overtimepay
    

def display(hours, payrate, grosspay, overtimepay, totalpay):
    print("Your total pay for this week is $" + str(totalpay))
    

def get_grosspay(hours, payrate):
    grosspay = hours * payrate
    
    
    return grosspay
    

def get_hours():
    print("Please enter the amount of hours worked this week.")
    hours = int(input())
    
    
    return hours
    

def get_payrate():
    print("Please enter your payrate.")
    payrate = int(input())
    
    
    return payrate
    

def get_total_grosspay(hours, payrate, grosspay, overtimepay):
    if hours > 40:
        overtimepay = calculate_overtime(hours, payrate)
        totalpay = overtimepay
    else:
        grosspay = get_grosspay(hours, payrate)
        totalpay = grosspay
        
    
    return totalpay
    

# Main


# My program calculates your gross pay for the week, taking into consideration any overtime worked automatically
while True:    #This simulates a Do Loop
    print("Will you need another calculation after this one? 1 = Yes 2 = No")
    again = int(input())
    hours = get_hours()
    payrate = get_payrate()
    overtimePay = calculate_overtime(hours, payrate)
    grosspay = get_grosspay(hours, payrate)
    totalpay = get_total_grosspay(hours, payrate, overtimePay, grosspay)
    display (hours, payrate, grosspay, totalpay, overtimePay)
    if not(again == 1): break   #Exit loop
