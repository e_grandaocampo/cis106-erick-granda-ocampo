def get_array():
    count = get_count()
    array1 = [None] * count
    for i in range(count):
        grade = get_grade()
        array1[i] = grade
    
    return array1
    
        
def get_count():
    print("How many grades will you enter?")
    count = int(input())
    
    return count
    
    
def get_grade():
    print("Enter a grade.")
    grade = int(input())
    
    return grade    
    
    
def get_average(array1):
    average = sum(array1) / len(array1)
    
    return average 
    
    
def display_results(array1, average):
    print ('Average: ', str(average))
    print ('Minimum: ', str(min(array1)))
    print ('Maximum: ', str(max(array1)))
    

def main():
    array1 = get_array()
    print(array1)
    average = get_average(array1)
    display_results(array1, average)
    
    
main()
