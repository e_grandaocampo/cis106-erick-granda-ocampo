# My program generates a list of multiplication expression using loop and by asking the user for the value and number of expressions.


def get_expressions():
    print("Please enter the number of expressions.")
    expressions = int(input())
    return expressions


def get_value():
    print("Please enter a value.")
    value = int(input())
    return value


def process(expressions, value):
    count = 1
    while count <= expressions:
        outcome = count * value
        print(str(value) + "*" + str(count) + "=" + str(outcome))
        count = count + 1


def main():
    value = get_value()
    expressions = get_expressions()
    process (value, expressions)


main()
