def getAmount():
    print("Please enter the amount of grades that will be entered.")
    amount = int(input())
    
    return amount

def getAverage(total, amount):
    average = float(total) / amount
    
    return average

def getGrade():
    print("Enter the grade.")
    grade = int(input())
    
    return grade

def output(total, amount):
    average = getAverage(total, amount)
    print("The average is " + str(average))

def process(amount):
    count = 1
    total = 0
    while count <= amount:
        grade = getGrade()
        total = grade + total
        count = count + 1
    
    return total

# Main

# My program finds the average of any number of grade
amount = getAmount()
total = process(amount)
output (total, amount)
