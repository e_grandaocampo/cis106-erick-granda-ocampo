def calculateovertime(hours, payrate):
    overtimepay = payrate * 40 + payrate * (hours - 40) * 1.5
    
    return overtimepay

def display(hours, payrate, grosspay, overtimepay, totalpay):
    print("Your total pay for this week is $" + str(totalpay))

def getgrosspay(hours, payrate):
    grosspay = hours * payrate
    
    return grosspay

def gethours():
    print("Please enter the amount of hours worked this week.")
    hours = int(input())
    
    return hours

def getpayrate():
    print("Please enter your payrate.")
    payrate = int(input())
    
    return payrate

def gettotalgrosspay(hours, payrate, grosspay, overtimepay):
    if hours > 40:
        overtimepay = calculateovertime(hours, payrate)
        totalpay = overtimepay
    else:
        grosspay = getgrosspay(hours, payrate)
        totalpay = grosspay
    
    return totalpay

# Main

# My program calculates your gross pay for the week, taking into consideration any overtime worked automatically
hours = gethours()
payrate = getpayrate()
overtimePay = calculateovertime(hours, payrate)
grosspay = getgrosspay(hours, payrate)
totalpay = gettotalgrosspay(hours, payrate, overtimePay, grosspay)
display (hours, payrate, grosspay, totalpay, overtimePay)
