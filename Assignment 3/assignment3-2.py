# My program calculates a person's age in months,days, hours and seconds.
# 
print("Enter your age")
age = int(input())
ageinmonths = age * 12
print("Your age in months is " + str(ageinmonths), end='', flush=True)
ageindays = age * 365
print(" ,your age in days is " + str(ageindays))
ageinhours = age * 8760
print("Your age in hours is " + str(ageinhours))
ageinseconds = age * 31536000
print("your age in seconds is " + str(ageinseconds))
