// My program calculates the employee's gross pay weekly,monthly and annualy.
Display "Enter the number of hour the employee worked."
Declare Integer hours

Input hours
Display "Enter payrate"
Declare Real payrate

Input payrate
Declare Integer weeklygrossPay

Set weeklygrossPay = hours * payrate
Display "The employees weekly gross pay is $", weeklygrossPay
Declare Integer monthlygrosspay

Set monthlygrosspay = hours * payrate * 4
Display "Employees monthly gross pay is $", monthlygrosspay
Declare Integer annualgrosspay

Set annualgrosspay = hours * payrate * 52
Display "Employees annual gross pay is $", annualgrosspay
