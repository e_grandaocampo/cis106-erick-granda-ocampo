# My program separates each item in the file into its own category. 

# References
# https://www.w3schools.com/python/python_ref_string.asp
# https://docs.python.org/2/tutorial/errors.html


def read_file():
    try:
        file = open('final.txt', 'r')
    except IOError:
        print("Can not open file")
    else:    
        lines = file.read()
        file.close()
        return lines 


def first_split(lines):
    try:
        lines = lines.split('<CD>')
        return lines 
    except:
        print("Invalid data")
        return False


def build_array(tag, lines):
    if lines == False:
        print("Missing File")
    else:
        array = []
        count = 0
        for line in lines:
            if count != 0:
                start = line.find(f"<{tag}>") + len(tag) + 2
                end = line.find(f"</{tag}>")
                array.append(line[start:end])
            count = count + 1 

        return array


def format_all(titles, artist, country, price, year):
    try:
        for i in range(len(titles)):
          print(f"{titles[i]} - {artist[i]} - {country[i]} - {price[i]} - {year[i]}")
    except:
        print("Error: missing fields")


def get_average(prices):
    try:
        for i in range(len(prices)):
          prices[i] = float(prices[i])   
    except: 
        print("Could not convert")
        return False
    else:   
        average = sum(prices)/len(prices)
  
        return average 


def the_average(average,prices):
    print("The average for all the titles $")
    print(round(average, 2))
    print("The amount of titles in this catalog is ")
    print(str(len(prices)))


def main():
    lines = read_file()
    page = first_split(lines)
    titles = build_array("TITLE", page)
    artist = build_array("ARTIST", page)
    country = build_array("COUNTRY", page)
    prices = build_array("PRICE", page)
    year = build_array("YEAR", page)
    average = get_average(prices)
    if average == False:
        print("Could not complete operation")
    else:
        format_all(titles, artist, country, prices, year)
        the_average(average, prices)
  

main()
