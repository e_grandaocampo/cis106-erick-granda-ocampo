# My program generates a list of multiplication expression using loop and by asking the user for the value and number of expressions.


def get_expressions():
    print("Please enter the value.")
    expressions = int(input())
    
    return expressions


def get_value():
    print("Please enter the number of expressions.")
    value = int(input())
    
    return value


def process(expressions, value):
    count = 1
    for count in range(count, expressions + 1, 1):
        outcome = count * value
        print(str(value) + "*" + str(count) + "=" + str(outcome))


# Main

value = get_value()
expressions = get_expressions()
process (value, expressions)
