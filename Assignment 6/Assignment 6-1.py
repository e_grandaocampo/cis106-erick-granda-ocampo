# My program converts your age into months, days, hours, seconds.


def get_age():
    print("Please enter your age")
    years = int(input())
    return years


def calculate_seconds(years):
    seconds = years * 365 * 24 * 60 * 60
    return seconds


def calculate_days(years):
    days = years * 365
    return days
    
    
def calculate_months(years):
    months = years * 12
    return months


def calculate_hours(years):
    hours = years * 365 * 24
    return hours


def display(years, days, months, seconds, hours):
    print("Your age in days is " + str(days))
    print("Your age in months is " + str(months))
    print("Your age in hours is " + str(hours))
    print("Your age in seconds is " + str(seconds))


def main():
    years = get_age()
    days = calculate_days(years)
    months = calculate_months(years)
    seconds = calculate_seconds(years)
    hours = calculate_hours(years)
    display(years, days, months, seconds, hours)


main()
