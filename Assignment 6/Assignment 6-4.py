# My program calculates the area for squares, retangles and parallelgrams.


def get_base():
    print("Please the base length")
    base = int(input())
    return base


def get_height():
    print("Please the height")
    height = int(input())
    return height


def calculate_square(base, height):
    square = base * height
    return square


def calculate_rectangle(base, height):
    rectangle = base * height
    return rectangle


def calculate_parallelogram(base, height):
    parallelogram = base * height
    return parallelogram


def display(parallelogram, square, rectangle):
    print("The area for the square is " + str(square))
    print("The area for the rectangle is " + str(rectangle))
    print("The area for the parallelogram is " + str(parallelogram))
   


def main():
    base= get_base()
    height= get_height()
    square = calculate_square(base, height)
    rectangle = calculate_rectangle(base, height)
    parallelogram = calculate_parallelogram(base, height)
    display (parallelogram, square, rectangle)


main()