# My program converts miles in yards, feet and inches.


def get_miles():
    print("Please enter the distance in miles")
    act_miles = int(input())
    return act_miles


def calculate_yards(act_miles):
    act_yards = act_miles * 1760
    return act_yards


def calculate_feet(act_miles):
    act_feet = act_miles * 5280
    return act_feet


def calculate_inches(act_miles):
    act_inches = act_miles * 63360
    return act_inches


def display(act_inches, act_yards, act_feet):
    print("Your miles in yards is " + str(act_yards))
    print("Your miles in yards is " + str(act_feet))
    print("Your miles in yards is " + str(act_inches))


def main():
    act_miles = get_miles()
    act_yards = calculate_yards(act_miles)
    act_feet = calculate_feet(act_miles)
    act_inches = calculate_inches(act_miles)
    display (act_inches, act_yards, act_feet)

main()
